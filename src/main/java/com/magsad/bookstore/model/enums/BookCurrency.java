package com.magsad.bookstore.model.enums;

public enum BookCurrency {
    AZN,USD,EUR,GBP,RUB,TRY
}
