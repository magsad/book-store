package com.magsad.bookstore.model.enums;

public enum BookStatus {
    PUBLISHED, UNPUBLISHED
}
