package com.magsad.bookstore.model.enums;

public enum BookType {
    SAVINGS_ACCOUNT, FIXED_DEPOSIT, LOAN_ACCOUNT
}
