package com.magsad.bookstore.model.entity;

public enum RoleName {
  ROLE_USER,
  ROLE_ADMIN,
  ROLE_PUBLISHER
}
