package com.magsad.bookstore.model.entity;

import com.magsad.bookstore.model.enums.BookStatus;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "books",uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
}
)
public class Book {
    @Id
    private String id;
    private String title;
    private String subtitle;
    private String description;
    private String language;
    private Long pageCount;
    @ManyToMany
    @ToString.Exclude
    private Set<Author> authors = new HashSet<>();
    @ManyToMany
    @ToString.Exclude
    private Set<Category> categories = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private BookStatus status;
    @ManyToOne
    private User publishedBy;

    private LocalDate publishDate;

    @CreationTimestamp
    private LocalDateTime openedAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public void addCategory(Category category) {
        categories.add(category);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Book book = (Book) o;
        return id != null && Objects.equals(id, book.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}
