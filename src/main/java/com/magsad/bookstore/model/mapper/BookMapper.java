package com.magsad.bookstore.model.mapper;

import com.magsad.bookstore.model.entity.Book;
import com.magsad.bookstore.payload.dto.BookCreateDTO;
import com.magsad.bookstore.payload.dto.BookResponseDTO;
import com.magsad.bookstore.payload.dto.BookUpdateDTO;
import com.magsad.bookstore.repository.UserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", uses = { UserRepository.class })
public abstract class BookMapper {

    public static final BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

//    @Mapping(source = "status", target = "status")
//    @Mapping(source = "availableBalance", target = "availableBalance")
//    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract Book convertToEntity(BookUpdateDTO accountUpdateDTO);

//    @Mapping(source = "number", target = "number")
//    @Mapping(source = "accountType", target = "accountType")
//    @Mapping(source = "accountCurrency", target = "accountCurrency")
//    @Mapping(source = "status", target = "status")
//    @Mapping(source = "availableBalance", target = "availableBalance")
//    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract Book convertRequestToEntity(BookCreateDTO accountCreateDTO);

//    @Mapping(source = "id", target = "id")
//    @Mapping(source = "number", target = "number")
//    @Mapping(source = "accountType", target = "accountType")
//    @Mapping(source = "accountCurrency", target = "accountCurrency")
//    @Mapping(source = "status", target = "status")
//    @Mapping(source = "availableBalance", target = "availableBalance")
//    @Mapping(source = "actualBalance", target = "actualBalance")
    public abstract BookResponseDTO convertToDTO(Book book);

//    @Mapping(source = "id", target = "id")
//    @Mapping(source = "number", target = "number")
//    @Mapping(source = "accountType", target = "accountType")
//    @Mapping(source = "accountCurrency", target = "accountCurrency")
//    @Mapping(source = "status", target = "status")
//    @Mapping(source = "availableBalance", target = "availableBalance")
//    @Mapping(source = "actualBalance", target = "actualBalance")
//    @Mapping(target = "user", ignore = true)
    public abstract List<BookResponseDTO> convertToDTOList(List<Book> books);
}
