package com.magsad.bookstore.service;

import com.magsad.bookstore.exception.BookNotFoundException;
import com.magsad.bookstore.exception.UserNotFoundException;
import com.magsad.bookstore.model.entity.Book;
import com.magsad.bookstore.model.entity.User;
import com.magsad.bookstore.model.enums.BookStatus;
import com.magsad.bookstore.model.mapper.BookMapper;
import com.magsad.bookstore.payload.dto.BookCreateDTO;
import com.magsad.bookstore.payload.dto.BookResponseDTO;
import com.magsad.bookstore.payload.dto.BookUpdateDTO;
import com.magsad.bookstore.repository.BookRepository;
import com.magsad.bookstore.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
@Slf4j
public class BookService {
    BookRepository bookRepository;
    UserRepository userRepository;

    public List<BookResponseDTO> getBooksByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        List<Book> books =  bookRepository.findByUser(user).stream().filter(book -> book.getStatus()== BookStatus.PUBLISHED).collect(Collectors.toList());
        return BookMapper.INSTANCE.convertToDTOList(books);
    }

    public BookResponseDTO getBookById(String id) {
        Book book = bookRepository.findById(id).get();
        return BookMapper.INSTANCE.convertToDTO(book);
    }

    public BookResponseDTO createAccount(BookCreateDTO accountCreateDTO) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(auth.getName());
        User user = userRepository.findByUsername(auth.getName()).orElseThrow(() -> new UserNotFoundException(auth.getName()));
        Book book = BookMapper.INSTANCE.convertRequestToEntity(accountCreateDTO);
        book.setPublishedBy(user);
        return BookMapper.INSTANCE.convertToDTO(bookRepository.save(book));

    }

    public BookResponseDTO updateAccount(BookUpdateDTO accountUpdateDTO) {
        Book book =  bookRepository.findById(accountUpdateDTO.getNumber()).orElseThrow(() -> new BookNotFoundException(accountUpdateDTO.getNumber()));
        log.info(book.getId().toString());
        book.setStatus(accountUpdateDTO.getStatus());
        return BookMapper.INSTANCE.convertToDTO(bookRepository.save(book));
    }

    public void deleteAccount(String number) {
        Book book =  bookRepository.findById(number).orElseThrow(() -> new BookNotFoundException(number));
        bookRepository.delete(book);
    }
}
