package com.magsad.bookstore.exception;

public class BookNotFoundException extends RuntimeException{
    public BookNotFoundException(String number) {
        super(String.format("Book not found with number = %s",number));
    }
}
