package com.magsad.bookstore.exception;

public class DenyException extends RuntimeException{
    public DenyException() {
        super("Deny! You can only see your accounts!");
    }
}
