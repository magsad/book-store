package com.magsad.bookstore.exception;

public class MustLogInException extends RuntimeException{
    public MustLogInException() {
        super("You must log in first!");
    }
}
