package com.magsad.bookstore.controller;

import com.magsad.bookstore.exception.MustLogInException;
import com.magsad.bookstore.model.entity.Book;
import com.magsad.bookstore.payload.dto.BookCreateDTO;
import com.magsad.bookstore.payload.dto.BookUpdateDTO;
import com.magsad.bookstore.payload.response.MessageResponse;
import com.magsad.bookstore.service.BookService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("book")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class BookController {
    BookService bookService;

/*    @GetMapping("query")
    @PreAuthorize("hasRole('USER') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public ResponseEntity getAccountsByUsernameQuery(@RequestParam String username,Principal principal) {
        try {
            if (!Objects.equals(principal.getName(), username)) {
                throw new DenyException();
            }
        }catch (NullPointerException e){
            throw new MustLogInException();
        }

        return ResponseEntity.ok(accountService.getAccountsByUsername(username));

    }*/

    @GetMapping("/{isbn}")
    public ResponseEntity get(@PathVariable("isbn") String isbn) {
        return ResponseEntity.ok(bookService.getBookById(isbn));
    }



//    @GetMapping("{username}")
//    @PreAuthorize("hasRole('USER') ")
//    public ResponseEntity getAccountsByUsername( @PathVariable String username) {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        log.info(auth.getName());
//        try {
//            if (!Objects.equals(auth.getName(), username)) {
//                return ResponseEntity.ok(new MessageResponse("Deny! You can only see your accounts!"));
//            }
//        }catch (NullPointerException e){
//            throw new MustLogInException();
//        }
//        return ResponseEntity.ok(bookService.getAccountsByUsername(username));
//    }

    @GetMapping("{username}/{number}")
    @PreAuthorize("hasRole('USER') ")
    public ResponseEntity getAccountByNumber(@PathVariable String username,
                                             @PathVariable String id,
                                             Principal principal) {
        try {
            if (!Objects.equals(principal.getName(), username)) {
                return ResponseEntity.ok(new MessageResponse("Deny! You can only see your accounts!"));
            }
        }catch (NullPointerException e){
            throw new MustLogInException();
        }

        return ResponseEntity.ok(bookService.getBookById(id));

    }

    @PostMapping
    @PreAuthorize("hasRole('USER') ")
    public ResponseEntity createAccount(@RequestBody BookCreateDTO accountCreateDTO){
        return ResponseEntity.status(HttpStatus.OK).body(bookService.createAccount(accountCreateDTO));
    }

    @PutMapping
    @PreAuthorize("hasRole('USER') ")
    public ResponseEntity updateAccount(@RequestBody BookUpdateDTO accountUpdateDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.updateAccount(accountUpdateDTO));
    }

    @DeleteMapping("{number}")
    @PreAuthorize("hasRole('USER') ")
    public ResponseEntity deleteAccount(@PathVariable String number) {
        bookService.deleteAccount(number);
        return ResponseEntity.status(HttpStatus.OK).body(new MessageResponse(String.format("Account with number = %s has been deleted!",number)));
    }
}

