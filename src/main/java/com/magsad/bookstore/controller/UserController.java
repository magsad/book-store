package com.magsad.bookstore.controller;

import com.magsad.bookstore.payload.request.RegisterRequest;
import com.magsad.bookstore.repository.UserRepository;
import com.magsad.bookstore.service.UserDetailsServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("user")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class UserController {
  UserRepository userRepository;
  UserDetailsServiceImpl userDetailsService;

  @GetMapping
  @PreAuthorize("hasRole('ADMIN') ")
  public ResponseEntity getUserList() {
    return ResponseEntity.ok(userDetailsService.loadAllUsers());
  }

  @GetMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  public ResponseEntity getUserById(@PathVariable Long id) {
      return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.loadUserById(id));
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  public ResponseEntity deleteUserById(@PathVariable Long id) {
    return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.deleteUserById(id));
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ADMIN') ")
  public ResponseEntity updateUserById(@PathVariable Long id,
                                       @RequestBody RegisterRequest registerRequest) {
    return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.updateUserById(id,registerRequest));
  }
}
