package com.magsad.bookstore.payload.dto;

import com.magsad.bookstore.model.enums.BookStatus;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BookUpdateDTO {
    private String number;
    private BookStatus status;
    private BigDecimal availableBalance;
    private BigDecimal actualBalance;
}
