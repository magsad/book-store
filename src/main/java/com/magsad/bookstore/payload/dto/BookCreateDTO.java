package com.magsad.bookstore.payload.dto;

import com.magsad.bookstore.model.enums.BookCurrency;
import com.magsad.bookstore.model.enums.BookStatus;
import com.magsad.bookstore.model.enums.BookType;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BookCreateDTO {
    private String number;
    private BookType accountType;
    private BookCurrency accountCurrency;
    private BookStatus status;
    private BigDecimal availableBalance;
    private BigDecimal actualBalance;
}
