package com.magsad.bookstore.repository;

import com.magsad.bookstore.model.entity.Book;
import com.magsad.bookstore.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book,String> {
    List<Book> findByUser(User user);
    Optional<Book> findById(String id);
}
